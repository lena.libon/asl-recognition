import cv2
import keras
import matplotlib.pyplot as plt
import numpy as np
import os
import tensorflow as tf

from keras.callbacks import ModelCheckpoint
from keras.layers import Conv2D, Dense, Dropout, Flatten, MaxPooling2D
from keras.models import Sequential
from sklearn.model_selection import train_test_split

np.random.seed(5)
tf.set_random_seed(2)
#%matplotlib inline #for Jupyter Notebook

train_dir = 'dataset/train'
eval_dir = 'dataset/test'

#Function to get the images (with their labels) from the directories
def get_images(dir):
    labels = []
    images = []
    for index, label in enumerate(label_code):
        for file in os.listdir(dir + '/' + label):
            filepath = dir + '/' + label + '/' + file
            image = cv2.resize(cv2.imread(filepath), (64,64))
            labels.append(index)
            images.append(image)
    labels = np.array(labels)
    images = np.array(images)
    return(images, labels)


label_code = sorted(os.listdir(train_dir))
trainimages, trainlabels = get_images(train_dir)
X_train, X_test, y_train, y_test = train_test_split(trainimages, trainlabels, test_size = 0.1, stratify = trainlabels)
X_eval, y_eval = get_images(eval_dir)

n_labels = len(label_code)
print('Number of labels: ', n_labels)
n_train_img = len(X_train)
print('Number of training images: ', n_train_img)
n_test_img = len(X_test)
print('Number of test images: ', n_test_img)
n_eval_img = len(X_eval)
print('Number of evaluation images: ', n_eval_img)

#normalize RGB values
X_train = X_train.astype(np.float32)/255.0
X_test = X_test.astype(np.float32)/255.0
X_eval = X_eval.astype(np.float32)/255.0

#one-hot encoding
y_train = keras.utils.to_categorical(y_train)
y_test = keras.utils.to_categorical(y_test)
y_eval = keras.utils.to_categorical(y_eval)

#Define and run model
model = Sequential()
model.add(Conv2D(filters = 64, kernel_size = 5, padding = 'same', strides = 1, activation = 'relu', input_shape = (64, 64,3)))
model.add(Conv2D(filters = 64, kernel_size = 5, padding = 'same', strides = 2, activation = 'relu'))
model.add(MaxPooling2D(4))
model.add(Dropout(0.5))
model.add(Conv2D(filters = 128, kernel_size = 5, padding = 'same', strides = 1, activation = 'relu'))
model.add(Conv2D(filters = 128, kernel_size = 5, padding = 'same', strides = 2, activation = 'relu'))
model.add(MaxPooling2D(4))
model.add(Dropout(0.5))
model.add(Flatten())
model.add(Dense(29, activation = 'softmax'))

model.summary()

#Compiling and fitting
model.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])

#Storing the optimal weights
checkpoint = ModelCheckpoint('current_model/weights.h5', monitor = 'val_acc', save_best_only = True, mode = 'max', verbose = 0)
callbacks_list = [checkpoint]

hist = model.fit(X_train, y_train, validation_data = (X_test, y_test), epochs = 1, batch_size = 64, callbacks = callbacks_list)

model.save('current_model/model.h5')

#Plotting training curves
print("Training curves:")
plt.figure(figsize = (20,10))
plt.subplot(1,2,1)
plt.suptitle('Optimizer = rmsprop', fontsize = 15)
plt.ylabel('Loss', fontsize = 16)
plt.plot(hist.history['loss'], label = "Training Loss")
plt.plot(hist.history['val_loss'], label = "Validation Loss")
plt.legend(loc = 'upper right')

plt.subplot(1,2,2)
plt.ylabel('Accuracy', fontsize = 16)
plt.plot(hist.history['acc'], label = 'Training Accuracy')
plt.plot(hist.history['val_acc'], label = 'Validation Accuracy')
plt.legend(loc = 'lower right')
plt.show()

score = model.evaluate(X_test, y_test, verbose = 0)
print('Accuracy of the test images: ', round(score[1] * 100, 3), '%')
score = model.evaluate(X_eval, y_eval, verbose = 0)
print('Accuracy of the evaluation images: ', round(score[1]*100, 3), '%')
