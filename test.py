import cv2
import numpy as np
import os
from keras.models import load_model

model = load_model('optimal_model/model.h5')
model.load_weights('optimal_model/weights.h5')
train_dir = 'dataset/train'
label_code = sorted(os.listdir(train_dir))

# Get a reference to the webcam
camera = cv2.VideoCapture(0)
camera_height = 800

while(True):
    # Read a new camera frame
    _, frame = camera.read()

    # Flip the frame vertically
    frame = cv2.flip(frame, 1)

    # Rescale the camera output
    aspect = frame.shape[1] / float(frame.shape[0])
    camera_width = int(aspect * camera_height)
    frame = cv2.resize(frame, (camera_width, camera_height))

    # Add a rectangle to the frame
    cv2.rectangle(frame, (500,200), (1000, 700), (0, 0, 255), 4)

    # Get region of interest (ROI), area inside of rectangle
    roi = frame[200+4:700-4, 500+4:1000-4]

    # Parse from BRG to RGB
    roi = cv2.cvtColor(roi, cv2.COLOR_BGR2RGB)

    # Normalize RGB values
    roi = roi.astype(np.float32)/255.0

    # Resize image
    roi = cv2.resize(roi, (64, 64))

    # Predict class
    roi_X = np.expand_dims(roi, axis = 0)
    predictions = model.predict(roi_X)
    inverted = np.argmax(predictions[0])
    inverted = label_code[inverted]

    # Add predicted label to frame
    cv2.putText(frame, inverted, (20,500), cv2.FONT_HERSHEY_SIMPLEX, 1.0, (240, 240, 240), 4)

    # Show the frame
    cv2.imshow('Predictor', frame)

    key = cv2.waitKey(1)

    # Close window if 'q' or 'esc' is pressed
    if key & 0xFF == ord('q') or key == 27:
        break;

camera.release()
cv2.destroyAllWindows()
