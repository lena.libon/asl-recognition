from flask import Flask, render_template, request
from keras.models import load_model
import numpy as np
from PIL import Image
import tensorflow as tf
import os 

#init flask app
app = Flask(__name__, template_folder='templates')

train_dir = 'dataset/train'
label_code = sorted(os.listdir(train_dir))

def init():
	global model, graph 
	#load the pre-trained Keras model 
	model = load_model('optimal_model/model.h5')
	model.load_weights('optimal_model/weights.h5')
	graph = tf.get_default_graph() 

@app.route('/')
def upload_file():
	return render_template('index.html')

@app.route('/uploader', methods = ['POST'])
def upload_image_file():
	if request.method == 'POST':
		img = Image.open(request.files['file'].stream).convert("L")
		if img.mode != "RGB":
			img = img.convert("RGB")
		img = img.resize((64, 64))
		img = np.array(img)
		img = img.astype(np.float32)/255.0
		img = np.expand_dims(img, axis = 0)
		with graph.as_default():
			y_pred = model.predict_classes(img) 

		return 'Predicted Letter: ' + label_code[y_pred[0]]

if __name__ == '__main__':
	print("Laoding model and starting server...")
	init()
	app.run(debug = True)